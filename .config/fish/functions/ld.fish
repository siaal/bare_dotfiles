# Defined in - @ line 1
function ld --wraps='exa -D --color=always' --description 'alias ld=exa -D --color=always'
  exa -D --color=always $argv;
end
