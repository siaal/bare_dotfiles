# Defined in - @ line 1
function ldr --wraps='ld -r' --description 'alias ldr=ld -r'
  ld -r $argv;
end
