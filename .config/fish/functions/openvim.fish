# Defined in - @ line 1
function openvim --wraps='alacritty -e nvim' --description 'alias openvim=alacritty -e nvim'
  alacritty -e nvim $argv;
end
