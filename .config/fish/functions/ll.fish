# Defined in - @ line 1
function ll --wraps='exa -al --color=always --group-directories-first' --description 'alias ll=exa -al --color=always --group-directories-first'
  exa -al --color=always --group-directories-first $argv;
end
