# Defined in - @ line 1
function laT --wraps='exa -aT --color=always --group-directories-first' --description 'alias laT=exa -aT --color=always --group-directories-first'
  exa -aT --color=always --group-directories-first $argv;
end
