-- local private = require("private") -- a lua file in home directory with private data
local theme_assets = require("beautiful.theme_assets")
local gears = require("gears")
local lain  = require("lain")
local awful = require("awful")
local wibox = require("wibox")
local dpi   = require("beautiful.xresources").apply_dpi
local calendar_widget = require("awesome-wm-widgets.calendar-widget.calendar") -- https://github.com/streetturtle/awesome-wm-widgets
local math, string = math, string
local os = os
local my_table = awful.util.table or gears.table -- 4.{0,1} compatibility

local theme                                     = {}

-- Dracula Colours
local background   = "#282a36"
local selection    = "#44475a"
local foreground   = "#f8f8f2"
local comment      = "#6272a4"
local cyan         = "#8be9fd"
local green        = "#50fa7b"
local orange       = "#ffb86c"
local pink 	   = "#ff79c6"
local purple 	   = "#bd93f9"
local red 	       = "#ff5555"
local yellow 	   = "#f1fa8c"

--not dracula but very pretty
local blue         = "#5e81ac"


theme.confdir                                   = os.getenv("HOME") .. "/.config/awesome/themes/siaal"
--theme.wallpaper                                 = theme.confdir .. "/wallpaper.jpg"
--theme.wallpaper                                 = "/usr/share/backgrounds/arcolinux/arco-wallpaper.jpg"
theme.main_wibar_pos                            = "bottom"
theme.font                                      = "Noto Sans Regular 11"
theme.taglist_font                              = "Noto Sans Regular 11"
theme.bg_normal                                 = background
theme.bg_focus                                  = selection
theme.bg_urgent                                 = comment
theme.fg_normal                                 = foreground
theme.fg_focus                                  = theme.fg_normal
theme.fg_urgent                                 = "#ffffff"
theme.fg_minimize                               = minimize
theme.border_width                              = dpi(2)
theme.border_normal                             = "#1c2022"
theme.border_focus                              = blue
theme.border_marked                             = "#3ca4d8"
theme.menu_border_width                         = 0
theme.menu_height                               = dpi(25)
theme.menu_width                                = dpi(260)
theme.menu_submenu_icon                         = theme.confdir .. "/icons/submenu.png"
theme.menu_bg_normal                            = theme.bg_normal
theme.menu_bg_focus                             = theme.bg_normal
theme.menu_fg_normal                            = foreground
theme.menu_fg_focus                             = purple
theme.widget_uptime                             = theme.confdir .. "/icons/ac.png"
theme.widget_cpu                                = theme.confdir .. "/icons/cpu.png"
theme.widget_fs                                 = theme.confdir .. "/icons/fs.png"
theme.widget_mem                                = theme.confdir .. "/icons/mem.png"
theme.widget_netdown                            = theme.confdir .. "/icons/net_down.png"
theme.widget_netup                              = theme.confdir .. "/icons/net_up.png"
theme.widget_mail                               = theme.confdir .. "/icons/mail.png"
theme.widget_batt                               = theme.confdir .. "/icons/bat.png"
theme.widget_clock                              = theme.confdir .. "/icons/clock.png"
theme.widget_vol                                = theme.confdir .. "/icons/spkr.png"
theme.widget_music                              = theme.confdir .. "/icons/note.png"
theme.widget_music_on                           = theme.confdir .. "/icons/note.png"
theme.widget_music_pause                        = theme.confdir .. "/icons/pause.png"
theme.widget_music_stop                         = theme.confdir .. "/icons/stop.png"
local taglist_square_size = dpi(5)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(taglist_square_size, foreground)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(taglist_square_size,foreground)
theme.tasklist_plain_task_name                  = false  --whether the icon has a plane for floating or a + for maximized etc
theme.tasklist_disable_icon                     = false
theme.useless_gap                               = 0
theme.layout_tile                               = theme.confdir .. "/icons/tilew.png"
theme.layout_tilegaps                           = theme.confdir .. "/icons/tilegapsw.png"
theme.layout_tileleft                           = theme.confdir .. "/icons/tileleftw.png"
theme.layout_tilebottom                         = theme.confdir .. "/icons/tilebottomw.png"
theme.layout_tiletop                            = theme.confdir .. "/icons/tiletopw.png"
theme.layout_fairv                              = theme.confdir .. "/icons/fairvw.png"
theme.layout_fairh                              = theme.confdir .. "/icons/fairhw.png"
theme.layout_spiral                             = theme.confdir .. "/icons/spiralw.png"
theme.layout_dwindle                            = theme.confdir .. "/icons/dwindlew.png"
theme.layout_max                                = theme.confdir .. "/icons/maxw.png"
theme.layout_fullscreen                         = theme.confdir .. "/icons/fullscreenw.png"
theme.layout_magnifier                          = theme.confdir .. "/icons/magnifierw.png"
theme.layout_floating                           = theme.confdir .. "/icons/floatingw.png"
theme.titlebar_close_button_normal              = theme.confdir .. "/icons/titlebar/close_normal.png"
theme.titlebar_close_button_focus               = theme.confdir .. "/icons/titlebar/close_focus.png"
theme.titlebar_minimize_button_normal           = theme.confdir .. "/icons/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus            = theme.confdir .. "/icons/titlebar/minimize_focus.png"
theme.titlebar_ontop_button_normal_inactive     = theme.confdir .. "/icons/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive      = theme.confdir .. "/icons/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active       = theme.confdir .. "/icons/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active        = theme.confdir .. "/icons/titlebar/ontop_focus_active.png"
theme.titlebar_sticky_button_normal_inactive    = theme.confdir .. "/icons/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive     = theme.confdir .. "/icons/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active      = theme.confdir .. "/icons/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active       = theme.confdir .. "/icons/titlebar/sticky_focus_active.png"
theme.titlebar_floating_button_normal_inactive  = theme.confdir .. "/icons/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive   = theme.confdir .. "/icons/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active    = theme.confdir .. "/icons/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active     = theme.confdir .. "/icons/titlebar/floating_focus_active.png"
theme.titlebar_maximized_button_normal_inactive = theme.confdir .. "/icons/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = theme.confdir .. "/icons/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active   = theme.confdir .. "/icons/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active    = theme.confdir .. "/icons/titlebar/maximized_focus_active.png"
local separator = wibox.widget.textbox(' <span color="' .. theme.fg_normal .. '">|</span>')
local spacer = wibox.widget.textbox(' <span color="' .. theme.fg_normal .. '"> </span>')

local markup = lain.util.markup

-- -- Textclock
-- os.setlocale(os.getenv("LANG")) -- to localize the clock
-- local clockicon = wibox.widget.imagebox(theme.widget_clock)
-- local mytextclock = wibox.widget.textclock(markup("#7788af", "%A %d %B ") .. markup("#535f7a", ">") .. markup("#de5e1e", " %H:%M "))
-- mytextclock.font = theme.font

-- Textclock
local clockicon = wibox.widget.imagebox(theme.widget_clock)
local mytextclock = awful.widget.watch(
    "date +'%a %d %b   %I:%M %p   '", 60,
    function(widget, stdout)
        widget:set_markup(" " .. markup.font(theme.font, stdout))
    end
)

-- -- Calendar - lain ver
-- theme.cal = lain.widget.cal({
--     attach_to = { mytextclock },
--     notification_preset = {
--         font = "Noto Sans Mono Medium 10",
--         message = message,
--         fg   = theme.fg_normal,
--         bg   = theme.bg_normal
--     }
-- })

-- Calendar -- awesomewmwidgets ver
-- https://github.com/streetturtle/awesome-wm-widgets
theme.cal = calendar_widget({
    -- theme = 'outrun',
    placement = 'bottom_right'
})
mytextclock:connect_signal("button::press", 
    function(_, _, _, button)
        -- if button == 1 then theme.cal.toggle() end
        theme.cal.toggle()
    end)


-- / fs
--[[ commented because it needs Gio/Glib >= 2.54
local fsicon = wibox.widget.imagebox(theme.widget_fs)
theme.fs = lain.widget.fs({
    notification_preset = { font = "Noto Sans Mono Medium 10", fg = theme.fg_normal },
    settings  = function()
        widget:set_markup(markup.fontfg(theme.font, "#80d9d8", string.format("%.1f", fs_now["/"].used) .. "% "))
    end
})
--]]

-- Mail IMAP check
--[[ commented because it needs to be set before use
local mailicon = wibox.widget.imagebox()
theme.mail = lain.widget.imap({
    timeout  = 180,
    server   = "server",
    mail     = "mail",
    password = "keyring get mail",
    settings = function()
        if mailcount > 0 then
            mailicon:set_image(theme.widget_mail)
            widget:set_markup(markup.fontfg(theme.font, "#cccccc", mailcount .. " "))
        else
            widget:set_text("")
            --mailicon:set_image() -- not working in 4.0
            mailicon._private.image = nil
            mailicon:emit_signal("widget::redraw_needed")
            mailicon:emit_signal("widget::layout_changed")
        end
    end
})
--]]

-- CPU
local cpuicon = wibox.widget.imagebox(theme.widget_cpu)
local cpu = lain.widget.cpu({
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, "#e33a6e", cpu_now.usage .. "% "))
    end
})


-- ALSA volume
local volicon = wibox.widget.imagebox(theme.widget_vol)
theme.volume = lain.widget.alsa({
    settings = function()
        if volume_now.status == "off" then
            volume_now.level = volume_now.level .. "M"
        end

        widget:set_markup(markup.fontfg(theme.font, "#FEFEFE", volume_now.level .. "% "))
    end
})


-- Net
local neticon = wibox.widget.imagebox(theme.widget_net)
local net = lain.widget.net({
        units=(1024^2)*2,
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, "#FEFEFE", " " .. net_now.received .. " ↓↑ " .. net_now.sent .. " "))
    end
})

-- MEM
local memicon = wibox.widget.imagebox(theme.widget_mem)
local memory = lain.widget.mem({
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, "#e0da37", mem_now.used .. "M "))
    end
})

---- MPD
--local musicplr = "audacious"
--local mpdicon = wibox.widget.imagebox(theme.widget_music)
--mpdicon:buttons(my_table.join(
--    awful.button({ modkey }, 1, function () awful.spawn.with_shell(musicplr) end),
--    --[[awful.button({ }, 1, function ()
--        awful.spawn.with_shell("mpc prev")
--        theme.mpd.update()
--    end),
--    --]]
--    awful.button({ }, 2, function ()
--        awful.spawn.with_shell("mpc toggle")
--        theme.mpd.update()
--    end),
--    awful.button({ modkey }, 3, function () awful.spawn.with_shell("pkill ncmpcpp") end),
--    awful.button({ }, 3, function ()
--        awful.spawn.with_shell("mpc stop")
--        theme.mpd.update()
--    end)))
--theme.mpd = lain.widget.mpd({
--    settings = function()
--        if mpd_now.state == "play" then
--            artist = " " .. mpd_now.artist .. " "
--            title  = mpd_now.title  .. " "
--            mpdicon:set_image(theme.widget_music_on)
--            widget:set_markup(markup.font(theme.font, markup("#FFFFFF", artist) .. " " .. title))
--        elseif mpd_now.state == "pause" then
--            widget:set_markup(markup.font(theme.font, " mpd paused "))
--            mpdicon:set_image(theme.widget_music_pause)
--        else
--            widget:set_text("")
--            mpdicon:set_image(theme.widget_music)
--        end
--    end
--})

-- Battery
local baticon = wibox.widget.imagebox(theme.widget_batt)
local bat = lain.widget.bat({
    settings = function()
        local perc = bat_now.perc ~= "N/A" and bat_now.perc .. "%" or bat_now.perc

        if bat_now.ac_status == 1 then
            perc = perc .. " plug"
        end

        widget:set_markup(markup.fontfg(theme.font, pink, perc .. " "))
    end
})

function theme.at_screen_connect(s)
    -- Quake application
    -- s.quake = lain.util.quake({ app = awful.util.terminal })
    s.quake = lain.util.quake({ app = "alacritty", height = 0.50, argname = "--name %s" })

    -- If wallpaper is a function, call it with the screen
    local wallpaper = theme.wallpaper
    if type(wallpaper) == "function" then
        wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, true)

    -- Tags
    awful.tag(awful.util.tagnames, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(my_table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, awful.util.taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, awful.util.tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position =theme.main_wibar_pos , screen = s, height = dpi(23), bg = theme.bg_normal, fg = theme.fg_normal })


    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            --s.mylayoutbox,
            s.mytaglist,
            separator,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            awful.widget.keyboardlayout(),
            separator,
            -- mpdicon,
            -- theme.mpd.widget,
            -- bat.widget,
            -- baticon,
            neticon,
            net.widget,
            volicon,
            theme.volume.widget,
            memicon,
            memory.widget,
            cpuicon,
            cpu.widget,
            separator,
            spacer,
            wibox.widget.systray(),
            mytextclock,
            spacer,
            s.mylayoutbox,
        },
    }

end

return theme
