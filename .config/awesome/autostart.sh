#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}
#run "xrandr --output HDMI2 --mode 1920x1080 --pos 1920x0 --rotate normal --output HDMI1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output VIRTUAL1 --off"
run "nm-applet"
run "pamac-tray"
# run "variety"
run xfce4-power-manager --no-daemon
run "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"
run "numlockx on"
run "volumeicon"
#you can set wallpapers in themes as well
# feh --bg-fill /usr/share/backgrounds/arcolinux/arco-wallpaper.jpg &
feh --bg-fill /usr/share/backgrounds/arcolinux/sea-whale.jpg &
#run applications from startup
run "dropbox"
#run "insync start"
#run "ckb-next -b"
# run "audacious"
run "discord"
run "redshift"

#run spotify
run /usr/bin/emacs --daemon &

# only if desktop user
if [ $USER = "siaal" ] ;
then
    run steam -silent
fi
