function fish_greeting; end # remove fish greeting
fish_vi_key_bindings               #vim keybinds

### EXPORTS ###
source $HOME/.config/fish/css_colours.fish # Standard CSS Colour Exports

set -x VIMCLIENT nvim
set -x GITUSER $USER
set -x REPOS $HOME/Repos

set -x PATH "$HOME/scripts:$HOME/.local/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/var/lib/snapd/snap/bin"

set -x CDPATH "./:$REPOS/gitlab.com/$GITUSER:$REPOS/github.com/$GITUSER:$REPOS/gitlab.com/*:$REPOS/github.com/*:$REPOS/gitlab.com:$REPOS/github.com:$REPOS:/media/$USER:$HOME"

# Here's your colored man pages right here.
set -x LESS_TERMCAP_mb $magen
set -x LESS_TERMCAP_md $yellow
set -x LESS_TERMCAP_me $reset
set -x LESS_TERMCAP_se $reset
set -x LESS_TERMCAP_so $cyan
set -x LESS_TERMCAP_ue $reset
set -x LESS_TERMCAP_us $violet
