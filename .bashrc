#
# ~/.bashrc
#

#Ibus settings if you need them
#type ibus-setup in terminal to change settings and start the daemon
#delete the hashtags of the next lines and restart
#export GTK_IM_MODULE=ibus
#export XMODIFIERS=@im=dbus
#export QT_IM_MODULE=ibus

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# all css colours as control codes
[[ -f ~/.config/bash/bash-css ]] && . ~/.config/bash/bash-css

# vim or die trying
set -o vi

###PROMPTS###
export PS1w=$'\033[35m'
export PS1u=$'\033[33m'
export PS1a=$'\033[38;2;100;100;100m'
export PS1c=$'\033[38;2;110;110;110m'
export PS1h=$'\033[34m'
export PS1p=$PS1u
export PS1P=$'\033[31m'
export PS1U=$PS1P

#PS1='[\u@\h \W]\$ ' # old default prompt
# NOTE that bash-prompt *ALSO* requires git-prompt for git functionality
if [[ -f ~/.config/bash/bash-prompt ]]; then
    source ~/.config/bash/bash-prompt 
elif [[ -f ~/.config/bash/git-prompt ]]; then
    source ~/.config/bash/git-prompt
    export PS1='\[${PS1u}\]\u\[${PS1c}\]@\[${PS1h}\]\h\[${PS1c}\]:\[${PS1w}\]\W$(__git_ps1 "\[${PS1c}\](\[${gruv_purple}\]%s\[${PS1c}\])")\[$PS1p\]$\[\033[00m\] '
else
    export PS1='\[${PS1u}\]\u\[${PS1c}\]@\[${PS1h}\]\h\[${PS1c}\]:\[${PS1w}\]\W\[$PS1p\]$\[\033[00m\] '
fi


###SHOPTS###
bind "set completion-ignore-case on" #ignore upper and lowercase when tab completing
shopt -s autocd         # change to named directory
shopt -s checkwinsize   # resize after each command if necessary
shopt -s cdspell        # autocorrects cd misspellings
shopt -s cmdhist        # save multi-line commands in history as single line
shopt -s dotglob        # include filenames beinning with a ` . ' in filename (wildcard) expansion
shopt -s expand_aliases # expand aliases
shopt -s extglob        #enables !(pattern1, pattern2) expansion for ?*+@!
shopt -s globstar       # ** will match all files, directories & subdiirectories
                        # if followed by a /, only (sub)directories will be matched
shopt -s histappend     # do not overwrite history
export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=5000


###EXPORTS###
if ( which nvim &>/dev/null); then export VIMCLIENT=nvim; else export VIMCLIENT=vim; fi
export VIMCLIENT=nvim        #i'm sorry rob, it has a nicer colourscheme
export REPOS=$HOME/Repos
export EDITOR=vim
export VISUAL=vim
export EDITOR_PREFIX=vim
export VIMSPELL=(~/.vim/spell/*.add)
if ( which git > /dev/null 2>&1 ); then test -z "${GITUSER}" && export GITUSER="$(git config --global user.name)"; fi
export PATH=$HOME/scripts:$HOME/.local/bin:$HOME/.emacs.d/bin:$HOME/bin:/usr/local/opt/coreutils/libexec/gnubin:/usr/local/bin:/usr/local/sbin:/usr/sbin:/usr/bin:/bin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl
export CDPATH=./:$HOME/Dropbox/work/:$REPOS/gitlab.com/$GITUSER:$REPOS/github.com/$GITUSER:$REPOS/github.com:$REPOS/gitlab.com:$REPOS:/media:$HOME

# Here's your colored man pages right here.
# CSS colour exports
export LESS_TERMCAP_mb=$magen
export LESS_TERMCAP_md=$yellow
export LESS_TERMCAP_me=$reset
export LESS_TERMCAP_se=$reset
export LESS_TERMCAP_so=$cyan
export LESS_TERMCAP_ue=$reset
export LESS_TERMCAP_us=$violet

# creates 'thefuck' alias
if ( which thefuck &>/dev/null ); then eval "$(thefuck --alias)"; fi

# lesspipe real nice
[ -x ~/.config/bash/lesspipe ] && eval "$(SHELL=/bin/sh ~/.config/bash/lesspipe)"

###ALIASES###
# docker
alias d='docker'
# CTF
how () { echo "$@" >> how.org; pwd; ls -l how.org; tail how.org; }
#vim
alias v=$VIMCLIENT
alias sv="sudo -E $VIMCLIENT"      # open Vim with sudo, but inheriting user config
alias openvim="alacritty -e $VIMCLIENT"                        # for vim from window manager and probably other things i guess?

#emacs
launch-emacs-daemon-bg () { emacsclient -c -a 'emacs' "$@" & }
alias e='launch-emacs-daemon-bg'
alias killemacs="emacsclient -e '(kill-emacs)'"
alias startemacs='/usr/bin/emacs --daemon &'
alias restartemacs="killemacs; startemacs"

alias td='grep "TODO" $HOME/Dropbox/org/agenda/todo.org'

alias killtheorphans='sudo pacman -Rns $(pacman -Qtdq)'
alias update='sudo pacman -Syyuu -y' #Update standard packages
alias upall='yay -Syyuu --noconfirm' # update standard and aur packages
alias yaysua='yay -Sua' #update only AUR packages

#pacman unlock
alias unlock="sudo rm /var/lib/pacman/db.lck"
alias rmpacmanlock="sudo rm /var/lib/pacman/db.lck"

# broot
alias br='br -dhp'
alias bs='br --sizes'

#ps (process list)
alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"

alias vifm='$HOME/.config/vifm/scripts/./vifmrun'

# open any file in any $PATH in vim
vic () { $VIMCLIENT $(which $1); }
alias info="info --vi-keys" # vi bindings for info
alias less="less -NR" # add line numbers to less # Force less to render ANSI colour codes
alias catn="cat -n" # add line numbers to cat

#ls shortcuts
alias l='ls --color=auto --group-directories-first'
# alias ls='ls --color=auto --group-directories-first' #leave ls unperverted
alias la='ls -A --color=always --group-directories-first'
alias ll='ls -lAh --color=auto --group-directories-first'
alias l.='ls -Alhd --color=auto --group-directories-first .* | egrep -v " \.[.]?$"'
alias lat="ls -At --color=auto"
alias latr="ls -Atr --color=auto"

alias mupdf='mupdf -I' # inverted mode

# qbittorrent wonk fix
alias qbit='export QT_AUTO_SCREEN_SCALE_FACTOR=0 && devour qbittorrent'

# wine aliases
alias killwine="wineserver -k"
alias forcekillwine="wineserver -k9"
alias dlwine="WINEPREFIX='/media/linux1tb/downloads/WINE' wine"


#hardware info --short
alias hw="hwinfo --short"

# Grep color
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

#Clear Screen
alias c='clear'
alias cl='clear; la'

# Adding flags
alias cp='cp -ir'        # confirm before overwriting something
alias df='df -h'         # human-readable sizes
alias mv='mv -i'         # confirm before overwriting something
alias rm='rm -I'         # confirm before removing a lot of things
alias free='free -mt'    # show sizes in MB

# Git
alias g="git"
alias privatedotfiles="/usr/bin/git --git-dir=$HOME/Repos/gitlab.com/$GITUSER/private-dotfiles/ --work-tree=/home/$USER"
export dotfilesasgit="/usr/bin/git --git-dir=$HOME/Repos/gitlab.com/$GITUSER/dotfiles/ --work-tree=/home/$USER"
alias dotfiles='$dotfilesasgit' # git alias for dotfiles repo
dotfiles-commit-add () { $dotfilesasgit add ${@:2}; $dotfilesasgit commit -m "$1"; }
git-commit-add () { git add ${@:2}; git commit -m "$1"; }
alias dotca='dotfiles-commit-add' # dotfiles add and commit, but with commit message first
alias gitca='git-commit-add'

alias sudo='sudo '                       # so that you can chain sudo into another alias
alias syserrors='sudo journalctl -p 3 -xb' 
alias sysderrors='sudo systemctl --failed'
alias ?='$HOME/scripts/./duck'
alias ??='$HOME/scripts/./google'
alias ???='$HOME/scripts/./bing'

#grub update
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

#add new fonts
alias update-fc='sudo fc-cache -fv'

#check vulnerabilities microcode
alias microcode='grep . /sys/devices/system/cpu/vulnerabilities/*'

#get fastest mirrors in your neighborhood
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

#mounting the folder Public for exchange between host and guest on virtualbox
alias vbm="sudo /usr/local/bin/arcolinux-vbox-share"

#Recent Installed Packages
alias rip="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -200 | nl"
alias riplong="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -3000 | nl"

#vim for important configuration files
#know what you do in these files
alias vbash="$VIMCLIENT ~/.bashrc"
alias vlightdm="sudo -E $VIMCLIENT /etc/lightdm/lightdm.conf"
alias vpacman="sudo -E $VIMCLIENT /etc/pacman.conf"
alias vgrub="sudo -E $VIMCLIENT /etc/default/grub"
alias vmkinitcpio="sudo -E $VIMCLIENT /etc/mkinitcpio.conf"
alias vslim="sudo -E $VIMCLIENT /etc/slim.conf"
alias voblogout="sudo -E $VIMCLIENT /etc/oblogout.conf"
alias vmirrorlist="sudo -E $VIMCLIENT /etc/pacman.d/mirrorlist"
alias vconfgrub="sudo -E $VIMCLIENT /boot/grub/grub.cfg"
alias bls="betterlockscreen -u /usr/share/backgrounds/arcolinux/"

# Who actually speedtests in bits?
alias speedtest="speedtest --bytes"
alias speedtestdl="speedtest --bytes --no-upload"

# R
alias R="R --quiet"

# No point disconnecting if you can't use internet
alias n="nordvpn" #shorter to type
alias nord="nordvpn" #shorter to type
alias nordoff="nordvpn s killswitch off; nordvpn d"
alias nordon="nordvpn c au; nordvpn s killswitch on"

#gpg
#verify signature for isos
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
#receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"

# furthur aliases that use specific programs like exa
[[ -f ~/.config/bash/bash_aliases ]] && . ~/.config/bash/bash_aliases

# Personal exports
[[ -f ~/.config/bash/bash-personal ]] && . ~/.config/bash/bash-personal

